#!/bin/bash

PROC=$(ps aux | grep '[c]offeee-app' | awk '{print $2}')

if test -n "$PROC" 
then
      echo "coffeee-app is running"
      echo "killing coffeee-app ..."
      kill $(ps aux | grep '[c]offeee-app' | awk '{print $2}')
else
      echo "coffeee-app is not running"
fi

echo "start coffeee-app..."

PORT=$(grep SERVER_PORT coffeee/.env | cut -d '=' -f2)

. ../venv/bin/activate && \
pip install -r requirements.txt && \
python manage.py collectstatic --noinput && \
gunicorn coffeee.wsgi:application -b 0:$PORT --workers=3 -n coffeee-app --daemon \
>> /var/log/coffeee.sys.log 2>&1

echo "coffeee-app started!"