from django.urls import path, include
# from django.conf.urls.static import static
# from django.conf import settings
# from django.contrib.auth.decorators import login_required
from web.views import *
from password_reset.views import *



urlpatterns = [
    path('', Homepage, name='homepage'),
    path('logout/', LogoutPage, name='logout'),
    path('403/', forbidden, name='forbidden'),
    path('profile/', show_profile, name='profile'),
    path('profile/edit/', update_profile, name='profile-edit'),
    path('profile/edit/password/', edit_password_profile, name='profile-edit-password'),
    path('take/', take_coffee, name='take'),
    path('waffles/', waffles, name='waffles'),
    path('coffee/', coffee, name='coffee'),
    path('coffee/pay/', pay_coffee, name='pay-coffee'),
    path('coffee/payments/', payment_coffee_list, name='pay-coffee'),
    path('consumption/confirm/', create_consumption, name='confirm-consumption'),
    path('consumption/delete/<id>/', delete_consumption, name='delete-consumption'),
    path('tos/', tos, name='tos'),
    path('privacy/', privacy, name='privacy'),
    ## password reset
    path('recover/<signature>', recover_done, name='password_reset_sent'),
    path('recover/', recover, name='password_reset_recover'),
    path('reset/done/', reset_done, name='password_reset_done'),
    path('reset/<token>[\w:-]/', reset, name='password_reset_reset'),
]
