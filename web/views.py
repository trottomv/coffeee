from django.shortcuts import render, redirect
from django.urls import reverse
from django.http import HttpResponseRedirect
from web.forms import *
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from coffeee.models import (
    Waffle,
    WaffleConsumption,
    WaffleConsumptionPayment,
    CoffeeeGroup,
)
from datetime import datetime
from django.db.models import Sum

from django.core.exceptions import PermissionDenied


# Create your views here.


# View for index page.
def Homepage(request, username = None):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/take')
    else:
        message = None
        if request.POST:
            anchor = 'login'
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username', None)
                password = form.cleaned_data.get('password', None)
                user = authenticate(username = username, password = password)
                if user:
                    login(request, user)
                    return redirect(reverse('take'))
                else:
                    message = {
                        'text': _("Username or password not valid"),
                        'type': 'error',
                    }
            else:
                message = {
                    'text': _("Username or password not valid"),
                    'type': 'error',
                }
            if message and message['type'] == 'error':
                return render(request, 'homepage.html', {'form' : form, 'message': message, 'anchor': anchor})
            else:
                return render(request, 'homepage.html', {'form' : form, 'message': message, 'anchor': None })
        else:
            form = LoginForm(request.POST)
            return render(request, 'homepage.html', {'form' : form, 'message': message, 'anchor': None })


def LogoutPage(request):
    logout(request)
    return redirect(reverse('homepage'))


def tos(request):
    return render(request, 'tos.html')


def privacy(request):
    return render(request, 'privacy.html')


def forbidden(request):
    return render(request, '403.html')


@login_required
def show_profile(request):
    show_user_form = UserEditForm(instance=request.user)
    show_profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request, 'profile.html', {
        'show_user_form': show_user_form,
        'show_profile_form': show_profile_form,
    })


@login_required
def update_profile(request):
    if request.method == 'POST':
        user_form = UserEditForm(request.POST, instance=request.user)
        profile_form = ProfileEditForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _('Your profile is successfully updated!'))
            return HttpResponseRedirect('/profile')

    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)

    return render(request, 'profile_edit.html', {
        'user_form': user_form,
        'profile_form': profile_form,
    })


@login_required
def edit_password_profile(request):
    message = None
    if request.method == 'POST':
        password_profile_form = ChangePasswordForm(request.POST, instance=request.user)
        if password_profile_form.is_valid():
            password_profile_form.save()
            # login the user again since the old session is invalid
            login(request, request.user, backend='django.contrib.auth.backends.ModelBackend')
            messages.success(request, _('Your password is successfully changed!'))
            return HttpResponseRedirect('/profile')
        else:
            message = {
                'text': _("Two password didn't match"),
                'type': 'error',
            }
    else:
        password_profile_form = ChangePasswordForm(instance=request.user)
    return render(request, 'profile_edit_password.html', {
        'password_profile_form': password_profile_form,
        'message': message,
    })


@login_required
def take_coffee(request):
    groups = request.user.profile.group.all()
    return render(request, 'take_coffee.html', {
        'groups': groups,
    })


@login_required
def waffles(request):
    group_req = request.GET.get('group') if request.GET.get('group') else 0
    groups = request.user.profile.group.all()
    groups_id = list(groups.values_list('id', flat=True))
    if group_req and int(group_req) in groups_id:
        groups = CoffeeeGroup.objects.filter(id=group_req)
    else:
        groups = None
        return render(request, '403.html')
    if groups:
        return render(request, 'waffles.html', {
            'groups': groups,
            'groups_id': groups_id,
        })


@login_required
def coffee(request):
    groups = request.user.profile.group.all()
    group_req = request.GET.get('group') if request.GET.get('group') else 0
    groups_id = list(groups.values_list('id', flat=True))

    if group_req and int(group_req) in groups_id:
        object_list = request.user.consumption.filter(waffle__group=int(group_req)).order_by('-date')
        group = request.user.profile.group.get(id=group_req)
    elif group_req and int(group_req) not in groups_id:
        return render(request, '403.html')
    else:
        object_list = request.user.consumption.all().order_by('-date')
        group = None
        
    limit = request.GET.get('limit') if request.GET.get('limit') else 0
    paginate_by = "5"
    consumptions_length = object_list.count()
    consumptions_paid = object_list.exclude(paid=False)
    consumptions_topay = object_list.exclude(paid=True)
    consumptions_total_amount = object_list.aggregate(Sum('waffle__price'))
    consumptions_paid_amount = consumptions_paid.aggregate(Sum('waffle__price'))
    consumptions_topay_amount = consumptions_topay.aggregate(Sum('waffle__price'))
    end = False if consumptions_length > int(limit) else True
    few = False if consumptions_length > int(paginate_by) else True
    if not limit:
        consumptions = object_list[:int(paginate_by)]
    else:
        consumptions = object_list[:int(limit)]

    return render(request, 'coffee.html', {
        'groups': groups,
        'group': group,
        'consumptions': consumptions,
        'paginate_by': paginate_by,
        'consumptions_length': consumptions_length,
        'consumptions_paid': consumptions_paid.count(),
        'consumptions_topay': consumptions_topay.count(),
        'amount_tot': round(consumptions_total_amount['waffle__price__sum'], 2) if consumptions_total_amount['waffle__price__sum'] else 0,
        'amount_paid': round(consumptions_paid_amount['waffle__price__sum'], 2) if consumptions_paid_amount['waffle__price__sum'] else 0,
        'amount_topay': round(consumptions_topay_amount['waffle__price__sum'], 2) if consumptions_topay_amount['waffle__price__sum'] else 0,
        'few': few,
        'end': end,
    })


@login_required
def create_consumption(request):
    user = request.user
    waffle_id = request.GET.get('waffle')
    if waffle_id:
        waffle = Waffle.objects.get(id=waffle_id)
        if request.method == 'POST':
            WaffleConsumption.objects.create(user=request.user, waffle=waffle, paid=False, date=datetime.now())
            return redirect(reverse('coffee'))
        else:
            return render(request, 'consumption_confirm.html', { 'waffle': waffle } )
    else:
        return render(request, 'consumption_confirm.html', { 'waffle': None } )


@login_required
def delete_consumption(request, id=None):
    user = request.user
    consumption = WaffleConsumption.objects.get(id=id)
    if user == consumption.user and not consumption.paid:
        if request.method == 'POST':
            consumption.delete()
            return redirect(reverse('coffee'))
        else:
            return render(request, 'consumption_delete.html', { 'consumption': consumption } )
    else:
        return redirect(reverse('forbidden'))


@login_required
def pay_coffee(request):
    groups = request.user.profile.group.all()
    group_req = request.GET.get('group') if request.GET.get('group') else 0
    groups_id = list(groups.values_list('id', flat=True))
    user = request.user
    
    if group_req and int(group_req) in groups_id:
        object_list = request.user.consumption.filter(waffle__group=int(group_req)).order_by('-date')
        group = request.user.profile.group.get(id=group_req)
    elif group_req and int(group_req) not in groups_id:
        return render(request, '403.html')
    else:
        object_list = []
        group = None
        
    consumptions_topay = object_list.exclude(paid=True) if object_list else 0
    consumptions_topay_amount = consumptions_topay.aggregate(Sum('waffle__price')) if consumptions_topay else {'waffle__price__sum': 0}

    if request.method == 'POST':
        request.user.consumption.filter(waffle__group=group_req).filter(paid=False).update(paid=True)
        WaffleConsumptionPayment.objects.create(user=user, date=datetime.now(), group=CoffeeeGroup.objects.get(id=group_req), amount=round(consumptions_topay_amount['waffle__price__sum'], 2))
        return redirect(reverse('coffee'))
    else:
        if consumptions_topay_amount['waffle__price__sum'] and round(consumptions_topay_amount['waffle__price__sum'], 2) > 0:
            return render(request, 'coffee_pay.html', {
                'consumptions_topay': consumptions_topay.count(),
                'amount_topay': round(consumptions_topay_amount['waffle__price__sum'], 2) if consumptions_topay_amount['waffle__price__sum'] else 0,
            })
        else:
            return redirect(reverse('forbidden'))


@login_required
def payment_coffee_list(request):
    groups = request.user.profile.group.all()
    group_req = request.GET.get('group') if request.GET.get('group') else 0
    groups_id = list(groups.values_list('id', flat=True))
    
    if group_req and int(group_req) in groups_id:
        object_list = request.user.payment.filter(group=int(group_req)).order_by('-date')
        group = request.user.profile.group.get(id=group_req)
        consumptions = request.user.consumption.filter(waffle__group=int(group_req)).order_by('-date')
    elif group_req and int(group_req) not in groups_id:
        return render(request, '403.html')
    else:
        object_list = []
        consumptions = request.user.consumption.all().order_by('-date')
        group = None
        
    limit = request.GET.get('limit') if request.GET.get('limit') else 0
    paginate_by = "5"
    payments_length = object_list.count()
    consumptions_paid = consumptions.exclude(paid=False)
    consumptions_paid_amount = consumptions_paid.aggregate(Sum('waffle__price'))
    end = False if payments_length > int(limit) else True
    few = False if payments_length > int(paginate_by) else True
    if not limit:
        payments = object_list[:int(paginate_by)]
    else:
        payments = object_list[:int(limit)]
        
    return render(request, 'coffee_payments.html', {
        'group': group,
        'payments': payments,
        'consumptions': consumptions,
        'paginate_by': paginate_by,
        'consumptions_paid': consumptions_paid.count(),
        'amount_paid': round(consumptions_paid_amount['waffle__price__sum'], 2) if consumptions_paid_amount['waffle__price__sum'] else 0,
        'few': few,
        'end': end,
    })