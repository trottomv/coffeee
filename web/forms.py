from django import forms
from django.contrib.auth import authenticate
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from coffeee.models import (
    Profile,
)
from django.contrib.auth.forms import UserCreationForm


class BootstrapForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(BootstrapForm, self).__init__(*args, **kwargs)
        # add common css classes to all widgets
        for field in iter(self.fields):
            #get current classes from Meta
            classes = self.fields[field].widget.attrs.get('class')
            if classes is not None:
                classes += ' form-control'
            else:
                classes = 'form-control'
                self.fields[field].widget.attrs.update({
                  'class': classes
                })


class BootstrapModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BootstrapModelForm, self).__init__(*args, **kwargs)
        # add common css classes to all widgets
        for field in iter(self.fields):
            #get current classes from Meta
            classes = self.fields[field].widget.attrs.get('class')
            if classes is not None:
                classes += ' form-control'
            else:
                classes = 'form-control'
                self.fields[field].widget.attrs.update({
                    'class': classes
                })


class LoginForm(BootstrapForm):
    username = forms.CharField(label = "Username", max_length = 150)
    password = forms.CharField(label = "Password", widget = forms.PasswordInput)
    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if not authenticate(username = username, password = password):
            raise forms.ValidationError(_("Username or Password is wrong."))
        return self.cleaned_data


class UserEditForm(BootstrapModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)

    def __init__(self, *args, **kwargs):
        super(UserEditForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)

    def clean_email(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return instance.email
        else:
            return self.cleaned_data['email']


class ProfileEditForm(BootstrapModelForm):

    class Meta:
        model = Profile
        fields = (
            'language',
            'avatar',
        )   


class ChangePasswordForm(UserCreationForm, BootstrapModelForm):

    class Meta:
        model = User
        fields = (
            'password1',
            'password2',
        )