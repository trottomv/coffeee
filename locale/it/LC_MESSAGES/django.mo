��    j      l  �   �      	     	     	     -	     A	  f   V	     �	     �	     �	     �	     �	     �	     �	     

     
     #
     1
     D
     K
     Q
     ^
     d
     l
  
   s
     ~
     �
     �
     �
     �
  C   �
     �
     �
  ^   �
     ^     f     o  L   �     �  
   �  	   �     �     �  	                  $     )     6     G     Y     m     r          �     �     �     �     �     �                    !     *     >     T     h     m     ~  k   �     �               +     0     ?     D     M     l     �     �  (   �  *   �     �  W   �  5   J     �     �     �     �  V   �  &   9     `  %   u     �     �     �     �     �     �     �     �     �               /  M  A     �     �     �     �  |   �     U     \     b     j     z     �  "   �     �     �     �     �     �     �     �                    #     (     >     E     L     Q  @   V     �     �  g   �               "  S   >     �     �     �     �     �  
   �     �     �     �     �               8     V     [  	   d     n     �     �     �     �     �  	   �  	   �                    1     J     `     f     }  �   �          !  !   2     T     \     m     t     }     �     �     �  2   �  0   �     .  R   1  >   �  *   �  #   �          .  C   ;  .        �  0   �     �               !     (     /     @     N     _     s  &   y     �         c   ;   &   /   g      ^      '          0   j   E   V       *   6   ?   S   K       C      G   !          Y       [          a       4      	   M   X   O      N           U   h               
   R          "   >       (   5       3                                       P   F   Z   <       1   8              ,             I   b   `   f   \       %   i   _          .      :       7       =   2   @                  #   d   J   Q   -             A      )      T   B   L       e                     9   W   $   H          ]   +                             D    403 Forbidden Administrator Administrator Group Administrators Group An email was sent to <strong>%(email)s</strong> %(ago)s ago. Use the link in it to set a new password. Avatar CUP Cancel Change Password Coffeee Confirm Confirm Delete Consumption Consumption Consumptions Creation Date Dear %(username)s, Delete ERROR Edit Profile Email English Female First Name Forgotten password? Group Groups Hi Hi  Hi, <strong>%(username)s</strong>. Please choose your new password. Home How many Coffee If you don't want to reset your password, simply ignore this email and it will stay unchanged. Is Paid Italiano Keep mind yours coffee Keep track of the coffees consumed in your shared office and manage payments Language Last Login Last Name Load More ... Login Login Now Login to your Logout Male New Password New password set No more coffee... No more payments... None Notification Notifications Password recovery Password recovery on %(domain)s Password recovery sent Pay now!!... Pay yours Coffee Pay yours coffee... Payment Payments Profile Profiles Recover my password Recover your password Repeat New Password Role Set new password Show details... Sorry, this password reset link is invalid. You can still <a href="%(recovery_url)s">request a new one</a>. TAKE ONE Take a Coffee Two password didn't match Undo Update Profile User Username Username or Password is wrong. Username or password not valid Waffle Waffles We are sorry, you can't take any coffee  We are sorry, you don't choiche any waffle Yes You -- or someone pretending to be you -- has requested a password reset on %(domain)s. You can set your new password by following this link: You don't made any payment yet You haven't had any coffee yet Your coffee list... Your logged in! Your password has successfully been reset. You can use it right now on the login page. Your password is successfully changed! Your payment list... Your profile is successfully updated! Yours Coffee gender how many coffee language members of coffee Paid of coffee TOT of coffee drink of coffee to Pay role take a break to work better your payment list Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-03-04 10:38+0100
PO-Revision-Date: 2020-03-04 10:50+0100
Last-Translator: 
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 403 Forbidden Amministratore Amministratore Gruppo Amministratori Gruppo Abbiamo spedito un email a <strong>%(email)s</strong> %(ago)s fa. Clicca sul link ricevuto per impostare una nuova password. Avatar TAZZE Annulla Cambia Password Coffeee Conferma Conferma eliminazione consumazione Consumazione Consumazioni Data di Creazione Caro %(username)s, Elimina ERRORE Modifica Profilo Email English Femmina Nome Password dimenticata? Gruppo Gruppi Ciao Ciao Ciao, <strong>%(username)s</strong>. Scegli una nuova password . Home Quanti Caffé Se non desideri reimpostare la tua password, ignora semplicemente questa email e non verrà modificata. Pagato Italiano Tieni a mente i tuoi caffé Tieni traccia dei caffè consumati nel tuo ufficio condiviso e registra i pagamenti Lingua Ultimo Login Cognome Carica di piú ... Accedi Accedi ora Accedi al tuo Disconnetti Maschio Nuova Password Nuova Password impostata nessun caffé ulteriore... Nessun pagamento ulteriore... None Notifica Notifiche Recupera la password Recupero password su %(domain)s Recupero password spedito Paga adesso!!... Paga i tuoi Caffé Paga i tuoi caffé Pagamento Pagamenti Profilo Profili Recupera la mia password Recupera la tua password Ripeti Nuova Password Ruolo Imposta Nuova Password Visualizza dettagli... Siamo dispiaciuti, questo link per il recupero password non é valido. Puoi <a href="%(recovery_url)s">richiedere un nuovo link</a>. SERVITI Prendi un caffé Le due password non corrispondono Annulla Aggiorna Profilo Utente Username Utente o Password non corretti. Username o password non validi Cialda Cialde Siamo dispiaciuti, non puoi prendere nessun caffé Siamo dispiaciuti, non hai scelto nessuna cialda Si Tu, o qualcun altro per te, ha richiesto il recupero della password su %(domain)s. Puoi impostare una nuova password cliccando sul seguente link: Non hai ancora effettuato nessun pagamento Non hai ancora preso nessun caffé  La lista dei tuoi caffé... Sei loggato! La tua password é stata reimpostata. Ora puoi usarla per accedere. La tua password é stata cambiata con successo La lista dei tuoi pagamenti... Il tuo profilo é stato aggiornato con successo! I tuoi Caffé genere quanti caffé lingua membri di caffé Pagate di caffé TOT di caffé bevute di caffé da Pagare ruolo prenditi una pausa per lavorare meglio la lista dei tuoi pagamenti 