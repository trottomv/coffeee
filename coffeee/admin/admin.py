from django.contrib import admin
from coffeee.models import (
    Profile, 
    CoffeeeGroup, 
    Waffle, 
    WaffleConsumption,
    WaffleConsumptionPayment,
    AdministratorGroup, 
    NotificationProfile,
)
from totalsum.admin import TotalsumAdmin

admin.site.register(Profile)
admin.site.register(CoffeeeGroup)
admin.site.register(Waffle)

@admin.register(WaffleConsumption)
class WaffleConsumptionAdmin(TotalsumAdmin):
    list_display = ('waffle', 'group', 'user', 'price', 'paid', 'date')
    search_fields = ('user', 'waffle')
    list_filter = ('user', 'waffle__group', 'waffle', 'paid')
    list_per_page = 10
    totalsum_list = ('price',)
    unit_of_measure = '&euro;'


admin.site.register(WaffleConsumptionPayment)
admin.site.register(AdministratorGroup)
admin.site.register(NotificationProfile)
