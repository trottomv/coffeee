from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from coffeee.models import CoffeeeGroup

import os
from datetime import datetime
from django.utils.timezone import now

def waffle_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<year>/<month>/<day>/<filename>
    return os.path.join("uploads",  instance.group.name, str(datetime.now().year), str(datetime.now().month), str(datetime.now().day), filename)

class Waffle(models.Model):
    class Meta:
        verbose_name = _("Waffle")
        verbose_name_plural = _("Waffles")
    
    name = models.CharField(max_length=50)
    group = models.ForeignKey(CoffeeeGroup, on_delete=models.PROTECT, related_name="waffle")
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to=waffle_directory_path, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return "[{}] {}".format(self.group, self.name)


class WaffleConsumption(models.Model):
    class Meta:
            verbose_name = _("Consumption")
            verbose_name_plural = _("Consumptions")

    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="consumption")
    waffle = models.ForeignKey(Waffle, on_delete=models.PROTECT)
    paid = models.BooleanField()
    date = models.DateTimeField(null=True)

    @property
    def price(self):
        return self.waffle.price

    @property
    def group(self):
        return self.waffle.group

    def __str__(self):
        return "[{}] [{}] {}".format(self.waffle.group, self.user.username, self.waffle.name)

class WaffleConsumptionPayment(models.Model):
    class Meta:
            verbose_name = _("Payment")
            verbose_name_plural = _("Payments")

    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="payment")
    group = models.ForeignKey(CoffeeeGroup, on_delete=models.PROTECT, related_name="group_payment", blank=True, null=True)
    amount = models.DecimalField(max_digits=6, decimal_places=2)
    date = models.DateTimeField(null=True)

    def __str__(self):
        return "[{}] {} - {}".format(self.user.username, self.amount, self.date)
