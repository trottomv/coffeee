from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from coffeee.models import Profile

import os
from datetime import datetime
from django.utils.timezone import now


def group_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<year>/<month>/<day>/<filename>
    return os.path.join("uploads",  instance.name, str(datetime.now().year), str(datetime.now().month), str(datetime.now().day), filename)


class CoffeeeGroup(models.Model):
    class Meta:
        verbose_name = _("Group")
        verbose_name_plural = _("Groups")
    
    name = models.CharField(max_length=50)
    members = models.ManyToManyField(Profile, related_name="group", verbose_name="User")
    owner = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    image = models.FileField(upload_to=group_directory_path, blank=True)


    def save(self, *args, **kwargs):
        return super().save(*args, **kwargs)

    def __str__(self):
        return "{}".format(self.name)


class AdministratorGroup(models.Model):
    class Meta:
        verbose_name = _("Administrator Group")
        verbose_name_plural = _("Administrators Group")

    user = models.ForeignKey(User, on_delete=models.PROTECT)
    group = models.ForeignKey(CoffeeeGroup, on_delete=models.PROTECT)

    def __str__(self):
        return "{} {}".format(self.user, self.group)