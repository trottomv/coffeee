from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User

from django.db.models.signals import post_save
from django.dispatch import receiver

import os
from datetime import datetime
import coffeee.settings as settings

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<year>/<month>/<day>/<filename>
    return os.path.join("uploads",  instance.user.username, str(datetime.now().year), str(datetime.now().month), str(datetime.now().day), filename)


class Profile(models.Model):
    class Meta:
        verbose_name = _("Profile")
        verbose_name_plural = _("Profiles")

    ROLE = (
        ('admin', _('Administrator')),
        ('staff', _('User')),
    )

    GENDER = (
        ('none', _('None')),
        ('male', _('Male')),
        ('female', _('Female')),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profile", verbose_name="User")
    gender = models.CharField(max_length=120, choices=GENDER, verbose_name=_("gender"))
    role = models.CharField(max_length=120, choices=ROLE, verbose_name=_("role"))
    avatar = models.FileField(upload_to=user_directory_path, blank=True)
    language = models.CharField(max_length=64, choices=settings.LANGUAGES, verbose_name=_("language"), default="IT")

    def __str__(self):
        return "{}".format(self.user)


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

class NotificationProfile(models.Model):
    class Meta:
        verbose_name = _("Notification")
        verbose_name_plural = _("Notifications")

    profile = models.ForeignKey(Profile, on_delete=models.PROTECT)
    _type = models.CharField(max_length=120)
    message = models.TextField()
    read = models.BooleanField()

    def __str__(self):
        return "{} {}".format(self.profile, self._type)
    